<?php

namespace Drupal\rh_profile\Plugin\RabbitHoleEntityPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rabbit_hole\Plugin\RabbitHoleEntityPluginBase;

/**
 * Implements rabbit hole behavior for profiles.
 *
 * @RabbitHoleEntityPlugin(
 *  id = "rh_profile",
 *  label = @Translation("Profile"),
 *  entityType = "profile"
 * )
 */
class Profile extends RabbitHoleEntityPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getGlobalConfigFormId() {
    return "profile_admin_settings";
  }

  /**
   * {@inheritdoc}
   */
  public function getGlobalFormSubmitHandlerAttachLocations(array $form, FormStateInterface $form_state) {
    return ['#submit'];
  }

}
