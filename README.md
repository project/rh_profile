# Rabbit Hole Profile
Integrates Rabbit Hole into Profile entities.

### Additional Requirements
* <a href="https://www.drupal.org/project/rabbit_hole">Rabbit Hole</a> 8.x-1.x or lower
* <a href="https://www.drupal.org/project/profile">Profile</a>